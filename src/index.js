'use strict';

const program = require('commander');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const url = 'mongodb://localhost:27017';

/**
 ** Для добавления пользователя необходимо использовать флаг --add. Пример: node index.js --add user1,user2,user3
 ** Для того, чтобы вывести список пользователей необходимо использовать флаг --show. Пример: node index.js --show
 ** Для изменения информации о пользователях нуэно использовать флаг --change. Пример node index.js --change user1,user2.newUser1,newUser2
 ** Для удаления пользователя нужно использовать флаг --remove. Пример node index.js --remove user1
 */

program
	.option('--add <names>', '', e => e.split(',').map(e => e.trim()))
	.option('--show')
	.option('--change <names>')
	.option('--remove <names>', '', e => e.split(',').map(e => e.trim()))
	.parse(process.argv);

MongoClient.connect(url, (err, client) => {
	if (err) {
		throw err;
	} else {		
		console.log('Connection established for', url);

		const db = client.db('test');
		const collection = db.collection('users');

		if (program.add) {
			if (program.add.length > 0) {
				let users = [];

				program.add.forEach(user => {
					users.push({name: user});
				});

				collection.insert(users, (err, result) => {
					if (err) {
						throw err;
					} else {
						console.log(`${program.add} added successfully`);
					}
				});
			} else {
				console.error('No users found. --add should contain at least 1 user');
			}
		}

		if (program.show) {
			collection.find().toArray((err, result) => {
				if (err) {
					throw err;
				} else if (result.length) {
					console.log(result);
				} else {
					console.log('Database empty');
				}
			});
		}

		if (program.change) {
			const data = program.change.split('.');

			let usersToChange = [];
			let newNamesOfUsers = [];

			if (data[1]) {
				usersToChange = data[0].split(',');
				newNamesOfUsers = data[1].split(',');
			} else {
				let split = data[0].split(',');

				usersToChange.push(split[0]);
				newNamesOfUsers.push(split[1]);
			}
			
			let i = 0;

			if (usersToChange.length !== newNamesOfUsers.length) {
				console.error('Names of users count is not equal. Example: name1,name2.newName1,newName2');
			}

			if (usersToChange.length <= 0) {
				console.error('Names can\'t be empty. Example: name1,name2.newName1,newName2');
			}

			usersToChange.forEach(username => {
				collection.updateMany({name: username}, {$set: {name: newNamesOfUsers[i]}}, (err, result) => {
					if (err) {
						throw err;
					}

					console.log(`${username} name changed`);
				});

				++i;
			});
		}

		if (program.remove) {
			if (program.remove.length > 0) {
				let users = [];

				program.remove.forEach(username => {
					collection.deleteMany({name: username}, (err, result) => {
						if (err) {
							throw err;
						} else {
							console.log(`${program.remove} removed successfully`);
						}
					});
				});				
			} else {
				console.error('No users found. --remove should contain at least 1 user');
			}
		}

		client.close();
	}
});